# Container expiration


Created some sub-images in the container registry:
    - registry.gitlab.com/rstyrczu/container-image-expiry/image1
    - registry.gitlab.com/rstyrczu/container-image-expiry/image2
    - registry.gitlab.com/rstyrczu/container-image-expiry/image3

Each image is tagged `latest` and `tmp_$COMMIT_SHA` on each commit.

On my local gitlab instance, I have similarly created images.

I've set up the repository's image expiration to expire on:
    - Interval 7 days
    - Run every day
    - Keep 1
    - Expire `.*`

My current expectation is for there to only remain tags `latest` and the most recent `tmp_$COMMIT_SHA` per image.

However, my experience is that none of the tags are cleaned up.
